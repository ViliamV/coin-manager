#!/usr/bin/env python3
import database as db
from tabulate import tabulate
from color_printer import color_h1, color_h2, color_ok, color_ko

def delete_transaction():
    print(color_h1('Delete transaction'))
    while True:
        transactions = db.get_all_transactions()
        if len(transactions) == 0:
            print(color_h2('No transactions'))
            break
        ids = [t[0] for t in transactions]
        print(tabulate(transactions, headers=(color_h2(x) for x in [
              'Id', 'From', 'To', 'Amount from', 'Amount to', 'Date'])))
        print()
        try:
            id_to_delete = int(input(color_h2('Index of transaction to delete: ')))
            if id_to_delete in ids:
                db.delete_transaction(id_to_delete)
                db.commit()
                print(color_ok('Transaction {} deleted.'.format(id_to_delete)))
            else:
                print(color_ko('Id not in range.'))
        except:
            print(color_ko('Not a number.'))
        another = input('Do you want to delete another transaction? [y/N] ')
        if another == '' or another[0] not in 'yY':
            break
    db.commit_and_close()
