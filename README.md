# Usage

```shell
./coin-manager.py --help
usage: coin-manager.py [-h] [--date DATE] [-r] [-l] [-a] [-p] [-d] [-g] [-f]
                       [--no-color] [--drop-cache]

This script manages investments in cryptocurrencies. It has several connectors
to exchanges and you can easily add more.

optional arguments:
  -h, --help   show this help message and exit
  --date DATE  Show last report from date
  -r           show most recent report
  -l           list reports
  -a           add transaction
  -p           print all data
  -d           delete transactions
  -g           show graph using matplotlib
  -f           force recalculation of graph values
  --no-color    print without color
  --drop-cache  delete cache table and exit
```

# Installation
1. Install necessary Python packages

    ```shell
    pip install -r requirements.txt --user
    ```

2. If you want to use graph you need `matplotlib` as well

    ```shell
    pip install -r requirements-grap.txt --user
    ```

3. Create your config file and edit default currencies

    ```shell
    cp coin-manager.conf.example coin-manager.conf
   ```

4. Setup your database

    ```shell
    sqlite3 database.db
    ```
    Then
    
    ```sql
    .read create_tables.sql
    .read create_exchanges.sql
    .exit
    ```

5. You are all done!

# User Guide / Documentation
In progress

# Images 
In progress
