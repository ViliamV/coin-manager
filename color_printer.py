import config

def color(string, color='white', bold=False, italic=False):
    if config.no_color:
        return string
    end = '\033[0m'
    colors = {
        'red': 31,
        'green': 32,
        'yellow': 33,
        'blue': 34,
        'magenta': 35,
        'cyan': 36,
        'white': 37
    }
    b = ';1' if bold else ''
    i = ';3' if italic else ''
    c = '\033[{}{}{}m'.format(colors[color], b, i)
    return c + string + end

def profit_color(number):
    if type(number) is str:
        return color(number)
    else:
        if number > 0:
            return color(str(number), 'green')
        else:
            return color(str(number), 'red')

def color_h1(string):
    return color(string, 'yellow', bold=True)

def color_h2(string):
    return color(string, 'blue', bold=True)

def color_ok(string):
    return color(string, 'green')

def color_ko(string):
    return color(string, 'red')
# # Test
# print(color('hello', 'red', bold=True))
# print(color('hello', 'green', italic=True))
# print(color('hello', 'yellow', bold=True, italic=True))
# print(color('hello', 'blue'))
# print(color('hello', 'magenta'))
# print(color('hello', 'cyan'))
# print(color('hello', 'white'))
