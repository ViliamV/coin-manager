#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from add_transaction import add_transaction
from datetime import datetime
from delete_transaction import delete_transaction
from print_data import print_data
from report import list_reports, get_report, generate_report
from database import drop_cache_table
import argparse
import config

parser = argparse.ArgumentParser(description='''This script manages investments in cryptocurrencies.\n
                                                It has several connectors to exchanges and you can easily add more.''')
parser.add_argument('--date', dest='date', type=str, default='',
                        help='Show last report from date')
parser.add_argument('-r', dest='recent', action='store_true',
                        help='show most recent report')
parser.add_argument('-l', dest='list', action='store_true',
                        help='list reports')
parser.add_argument('-a', dest='add', action='store_true',
                        help='add transaction')
parser.add_argument('-p', dest='print', action='store_true',
                        help='print all data')
parser.add_argument('-d', dest='delete', action='store_true',
                        help='delete transactions')
parser.add_argument('-g', dest='graph', action='store_true',
                        help='show graph using matplotlib')
parser.add_argument('-f', dest='force', action='store_true',
                        help='force recalculation of graph values')
parser.add_argument('--no-color', dest='no_color', action='store_true',
                        help='print without color')
parser.add_argument('--drop-cache', dest='drop_cache', action='store_true',
                        help='delete cache table and exit')


ARGS = parser.parse_args()
add = ARGS.add
delete = ARGS.delete
force = ARGS.force
from_date = ARGS.date
graph = ARGS.graph
list_r = ARGS.list
print_data_arg = ARGS.print
recent = ARGS.recent
config.no_color = ARGS.no_color
drop_cache = ARGS.drop_cache
NOW = datetime.now().isoformat()

if __name__  == '__main__':
    if drop_cache:
        drop_cache_table()
    elif list_r:
        list_reports()
    elif add:
        add_transaction()
    elif recent:
        get_report(NOW)
    elif graph:
        import graph as g
        g.generate(force_redraw=force)
    elif force and not graph:
        print('Option -f is only valid with -g')
    elif from_date != '':
        get_report(from_date)
    elif print_data_arg:
        print_data()
    elif delete:
        delete_transaction()
    else:
        generate_report(NOW)
    # db.commit_and_close()
    exit(0)
