#!/usr/bin/env python3
from datetime import datetime
import database as db
from tabulate import tabulate
from color_printer import color, profit_color, color_h1, color_h2

def print_data():
    print(color_h1('Transactions'))
    primary_real = db.get_currency_ticker(db.get_primary_real())
    trans_headers = (color_h2(x) for x in ['From', 'To', 'Amount from',
                     'Amount to', 'Date', 'Price in {}'.format(primary_real)])
    transactions = list(map(lambda x: x[1:], db.get_all_transactions()))
    print(tabulate(transactions, headers=trans_headers))
    print()

    print(color_h1('Portfolio'))
    port_headers = (color_h2(x) for x in ['Asset', 'Amount'])
    portfolio = []
    for c in db.get_all_currencies():
        portfolio.append(
            [color(c[2], 'blue'), profit_color(db.get_currency_state(datetime.now().isoformat(), c[0]))])
    print(tabulate(portfolio, headers=port_headers))
    db.commit_and_close()
