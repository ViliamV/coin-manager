import krakenex
import os


def exchange(c_from, c_to, amount):
    k = krakenex.API()
    directory = os.path.dirname(os.path.realpath(__file__))
    k.load_key('{}/kraken.key'.format(directory))

    if c_to == 'btc':
        c_to = 'XBT'
    pair = {'pair': 'X' + c_from.upper() + 'X' + c_to.upper()}
    success = False
    while not success:
        try:
            response = k.query_public('Ticker', pair)
            success = True
        except:
            pass
        finally:
            pass
    return float(response['result'][pair['pair']]['c'][0])*amount
