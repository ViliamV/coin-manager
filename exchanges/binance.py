from urllib.request import urlopen, Request
import json

def exchange(c_from, c_to, amount):
    url = 'https://api.binance.com/api/v1/ticker/24hr?symbol={}{}'.format(c_from.upper(), c_to.upper())
    request = Request(url)
    response_body = urlopen(request).read()
    body = json.loads(response_body.decode('utf-8'))
    return float(body['bidPrice']) * amount
