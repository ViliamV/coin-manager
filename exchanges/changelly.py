import hashlib
import hmac
import json
import requests
import os


API_URL = 'https://api.changelly.com'

def exchange(curr_from, curr_to, amount):
    directory = os.path.dirname(os.path.realpath(__file__))
    config = {}
    with open('{}/changelly.key'.format(directory)) as f:
        for line in f:
            key, value = line.split('=')
            config[key.strip()] = value.strip()
    if 'API_SECRET' in config and 'API_KEY' in config:
        message = {
            'jsonrpc': '2.0',
            'id': 1,
            'method': 'getExchangeAmount',
            'params': {
                'from': curr_from,
                'to': curr_to,
                'amount': str(round(amount, 10))}
        }

        serialized_data = json.dumps(message)

        sign = hmac.new(config['API_SECRET'].encode('utf-8'), serialized_data.encode('utf-8'), hashlib.sha512).hexdigest()

        headers = {'api-key': config['API_KEY'], 'sign': sign, 'Content-type': 'application/json'}
        response = requests.post(API_URL, headers=headers, data=serialized_data)

        return float(response.json()['result'])
    else:
        raise Exception('No API keys for changelly')
