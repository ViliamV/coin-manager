from urllib.request import urlopen, Request
import json
import re

def exchange(c_from, c_to, amount):
    url = 'https://spectrocoin.com/scapi/ticker/{}/{}'.format(c_from.upper(), c_to.upper())
    request = Request(url)
    response_body = urlopen(request).read()
    body = json.loads(response_body.decode('utf-8'))
    return amount / float(body['last'])
