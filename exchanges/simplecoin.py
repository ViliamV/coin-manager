from urllib.request import urlopen, Request
import re

def exchange(c_from, c_to, amount):
    simplecoin = 'http://simplecoin.cz'
    sell_string = 'offer: \d*'
    a = urlopen(simplecoin)
    bitcoin_sell_price = re.search(sell_string, str(a.read())).group(0).split(' ')[1]
    return float(bitcoin_sell_price) * amount
