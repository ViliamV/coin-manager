from urllib.request import urlopen, Request
import json
import re

def exchange(c_from, c_to, amount):
    url = 'https://api.coinbase.com/v2/exchange-rates?currency={}'.format(c_from.upper())
    request = Request(url)
    response_body = urlopen(request).read()
    body = json.loads(response_body.decode('utf-8'))
    return amount * float(body['data']['rates'][c_to.upper()])
