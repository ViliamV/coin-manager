from bittrex import Bittrex
import urllib
import requests

def request(param):
    options = {'market': param}
    method = 'getticker'
    base_url = 'https://bittrex.com/api/v1.1/%s/'
    request_url = (base_url % 'public') + method + '?'
    request_url += urllib.parse.urlencode(options)
    ret = requests.get(request_url)
    return ret.json()

def exchange(c_from, c_to, amount):
    counter = 0
    max_counter = 5
    while counter < max_counter:
        try:
            response = request(c_to.upper()+'-'+c_from.upper())
            return float(response['result']['Last'])*amount
        except:
            counter += 1
    return 0
