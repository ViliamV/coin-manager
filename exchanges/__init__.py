__all__ = [
    'binance',
    'brex',
    'changelly',
    'coinbase',
    'coinmate',
    'kraken',
    'simplecoin',
    'spectrocoin',
    ]
