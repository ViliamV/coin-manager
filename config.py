from os import path
import toml

directory = path.dirname(path.realpath(__file__))
try:
    config = toml.load('{}/coin-manager.conf'.format(directory))
except:
    print('No config found!\nCopy {}/coin-manager.conf.example to {}/coin-manager.conf'.format(directory, directory))
    config = {
        'NO_COLOR': False,
        'THRESHOLD': 0.01,
        'PRIMARY_REAL': 'czk',
        'PRIMARY_CRYPTO': 'btc',
        'COLUMNS': [ "ticker", "amount", "in_primary_crypto", "in_primary_real", "invested_in_primary_real", "percent", "portion" ]
    }
finally:
    no_color = config['NO_COLOR']
    threshold = config['THRESHOLD']
    primary_real = config['PRIMARY_REAL']
    primary_crypto = config['PRIMARY_CRYPTO']
    columns = config['COLUMNS']
