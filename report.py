from color_printer import color, profit_color, color_h1, color_h2, color_ko
from os import path
from tabulate import tabulate
import config
import database as db
import toml
import os

def profit(num, denom):
    if denom != 0:
        return profit_color(int(num / denom))
    else:
        return 'N/A'

def get_report(date):
    try:
        r_date, report = db.get_report(date)
        report = [list(x) for x in report]
        print_report(report, r_date)
    except:
        print(color_ko('No report from {}'.format(date)))

def list_reports():
    print(color_h1('Reports'))
    reports = [[y] for y in sorted([x[1] for x in db.get_all('reports')])]
    print(tabulate(reports))

def generate_report(date):
    from exchange import exchange
    report = []
    report_id = db.add_report(date)
    for id, currency, source in db.get_currencies_for_report():
        original_amount = db.get_currency_state(date, id)
        if original_amount >= config.threshold:
            try:
                if currency == config.primary_crypto:
                    exchanged = exchange(source, currency, config.primary_real, original_amount)
                else:
                    exchanged= exchange(source, currency, config.primary_crypto, original_amount)
            except:
                exit(1)
            db.add_row(report_id, id, original_amount, exchanged)
            report.append([id, currency, original_amount, exchanged])
    db.commit()

def columns_to_headers():
    formated = []
    formats = []
    for c in config.columns:
        if c == 'in_primary_real':
            formated.append('in {}'.format(config.primary_real))
            formats.append('.2f')
        elif c == 'in_primary_crypto':
            formated.append('in {}'.format(config.primary_crypto))
            formats.append('.4f')
        elif c == 'invested_in_primary_real':
            formated.append('invested in {}'.format(config.primary_real))
            formats.append('.2f')
        elif c == 'percent':
            formated.append('profit in %')
            formats.append('.0%')
        elif c == 'portion':
            formated.append('% of portfolio')
            formats.append('.1%')
        elif c == 'profit':
            formated.append('profit in {}'.format(config.primary_real))
            formats.append('.2f')
        elif c == 'amount':
            formated.append('amount')
            formats.append('.4f')
        else:
            formated.append(c)
            formats.append('.0f')
    headers = []
    for f in formated:
        headers.append(color_h2(f))
    return headers, formats

def print_report(report, date):
    # report = [(id, ticker, amount, exchanged amount)]
    print(color_h1('Report from ' + date), end='\n\n')
    report_data = [[] for i in range(len(report))]
    pr_pc = 0
    total_in_primary_crypto = 0
    total_in_primary_real = 0
    for r in report:
        if r[1] == config.primary_crypto:
            pr_pc = r[3] / r[2]
            total_in_primary_crypto += r[2]
        else:
            total_in_primary_crypto += r[3]
    for row, r in zip(report_data, report):
        id, ticker, amount, exchanged = r
        if ticker == config.primary_crypto:
            in_pc = amount
            in_pr = exchanged
        else:
            in_pc = exchanged
            in_pr = exchanged * pr_pc
        invested = db.get_currency_in_real(date, id)
        invested = max(0, invested)
        total_in_primary_real += in_pr
        for column in config.columns:
            if column == 'ticker':
                row.append(color(ticker, 'blue'))
            if column == 'amount':
                row.append(amount)
            if column == 'in_primary_crypto':
                row.append(in_pc)
            if column == 'in_primary_real':
                row.append(in_pr)
            if column == 'invested_in_primary_real':
                row.append(invested)
            if column == 'percent':
                row.append(profit(100 * ((in_pr) - invested), invested))
            if column == 'profit':
                row.append(profit_color(in_pr - invested))
            if column == 'portion':
                row.append(in_pc / total_in_primary_crypto)

    headers, formats = columns_to_headers()
    print(tabulate(report_data, headers=headers,
                floatfmt=formats), end='\n\n')
    invested_total = -db.get_currency_state(date, db.get_primary_real())
    print(color_h1('Totals'))
    print(tabulate([
        [color('Exchanged to {}'.format(config.primary_crypto), 'blue'), total_in_primary_crypto],
        [color('Exchanged to {}'.format(config.primary_real), 'blue'), total_in_primary_real],
        [color('Invested in {}'.format(config.primary_real), 'blue'), invested_total]
    ], tablefmt='plain', floatfmt='.2f'), end='\n\n')
    print(color_h1('Potential profit in {}'.format(config.primary_real)), profit_color(round(total_in_primary_real - invested_total)))
    db.commit_and_close()
