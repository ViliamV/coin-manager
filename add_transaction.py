#!/usr/bin/env python3
from datetime import datetime
from color_printer import color_h1, color_h2, color_ok, color_ko
import database as db

def valid_float(s):
    try:
        float(s)
        return True
    except:
        return False

def add_exchange(exchange):
    print(color_ok('Adding exchange {}. Make sure that you added the exchange connector.'.format(exchange)))
    return db.add_exchange(exchange)

def print_exchanges():
    print(color_h1('Exchanges'))
    for exchange in db.get_all_exchanges():
        print('{} - {}'.format(exchange[0], exchange[1]))


def add_currency(currency):
    def check_db(input_string):
        while True:
            field = input(input_string)
            if db.get_currency_id(field) is None:
                break
            else:
                print(color_ko('Currency is already in the database.'))
        return field

    while True:
        name = check_db(color_h2('Currency name: '))
        ticker = check_db(color_h2('Currency ticker: '))
        if name.lower() == currency.lower() or ticker.lower() == currency.lower():
            break
        else:
            print('Name nor ticker match the currency you entered before, please try again.')
    notes =  input(color_h2('Investment note (such as name of the wallet): '))
    print_exchanges()
    while True:
        exchange_id = None
        exchange = input(color_h2('Exchange for currency (leave empty if none): '))
        if exchange != '':
            exchange_id = db.get_exchange_id(exchange)
            if exchange_id is None:
                print(color_ko('Unknown exchange'))
                decision = input(color_h2('Would you like to add it? [Y/n] '))
                if decision == '' or decision[0] in 'yY':
                    exchange_id = add_exchange(exchange)
                    break
            else:
                break
        else:
            break
    update = 'Adding {} ({})'.format(name, ticker)
    update += ' with' if notes != '' or exchange != '' else ''
    update += ' note {}'.format(notes) if notes != '' else ''
    update += ' and' if notes != '' and exchange != '' else ''
    update += ' exchange {}'.format(exchange) if exchange != '' else ''
    print(color_ok(update))
    return db.add_currency(name, ticker.lower(), notes, exchange_id)


def add_transaction():
    print(color_h1('Add transaction'))
    options = ['from', 'to']
    while True:
        transaction = []
        for option in options:
            while True:
                investment = input(color_h2('Investment {}: '.format(option)))
                currency_id = db.get_currency_id(investment.lower())
                if currency_id is None:
                    print(color_ko('This currency is not in porfolio.'))
                    decision = input('Would you like to add it? [Y/n] ')
                    if decision == '' or decision[0] in 'yY':
                        currency_id = add_currency(investment)
                        transaction.append(currency_id)
                        break
                else:
                    transaction.append(currency_id)
                    break
        for option in options:
            while True:
                amount = input(color_h2('Amount {}: '.format(option)))
                if valid_float(amount):
                    transaction.append(float(amount))
                    break
                else:
                    print(color_ko('Invalid number.'))
        while True:
            date = input(color_h2('Date of transaction (ISO8601 or \"[t]oday\"): '))
            if date != '':
                if date[0] in 'tT':
                    date = datetime.now().isoformat()
                break
        transaction.append(date)
        confirm = input('Would you like to add transaction from {} {} to {} {} on date {}? [Y/n] '.format(
            transaction[2],db.get_currency_ticker(transaction[0]),transaction[3],db.get_currency_ticker(transaction[1]),transaction[4]
            ))
        if confirm == '' or confirm[0] in 'yY':
            db.add_transaction(*transaction)
            db.commit()
        another = input('Would you like to add another transaction? [y/N] ')
        if another == '' or another[0] not in 'yY':
            break
    db.commit_and_close()
