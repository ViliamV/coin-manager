---
--- Exchanges
---
CREATE TABLE exchanges (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);
---
--- Currencies
---
CREATE TABLE currencies (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  ticker TEXT NOT NULL,
  notes TEXT,
  exchange_id INTEGER,
  FOREIGN KEY(exchange_id) REFERENCES exchanges(id)
);

---
--- Transactions
---
CREATE TABLE transactions (
  id INTEGER PRIMARY KEY,
  currency_from INTEGER,
  currency_to INTEGER,
  amount_from REAL NOT NULL,
  amount_to REAL NOT NULL,
  date TEXT NOT NULL,
  FOREIGN KEY(currency_from) REFERENCES currencies(id),
  FOREIGN KEY(currency_to) REFERENCES currencies(id)
);

---
--- Reports
---
CREATE TABLE reports (
  id INTEGER PRIMARY KEY,
  date TEXT NOT NULL
);

---
--- Report rows
---
CREATE TABLE report_rows (
  id INTEGER PRIMARY KEY,
  report_id INTEGER,
  currency_id INTEGER,
  amount REAL NOT NULL,
  exchanged REAL NOT NULL,
  FOREIGN KEY(report_id) REFERENCES reports(id),
  FOREIGN KEY(currency_id) REFERENCES currencies(id)
);
