from exchanges import *

EXCHANGE = {
        'binance': binance,
        'bittrex': brex,
        'changelly': changelly,
        'coinbase': coinbase,
        'coinmate': coinmate,
        'kraken': kraken,
        'simplecoin': simplecoin,
        'spectrocoin': spectrocoin}

def exchange(source, c_from, c_to, amount=1):
    # print(c_from, c_to, amount)
    # if c_from in ['dash', 'eth', 'etc', 'ltc', 'rep', 'zec']:
    #     print(c_from, c_to, amount)
    #     print(f'simplecoin: {EXCHANGE[source].exchange(c_from, c_to, amount)}')
    #     print(f'kraken: {EXCHANGE["kraken"].exchange(c_from, c_to, amount)}')
    return EXCHANGE[source].exchange(c_from, c_to, amount)
