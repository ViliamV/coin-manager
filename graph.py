import database as db
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os.path
import pickle
import dateutil.parser as parser
import config

GRAPH_DATA_FOLDER = os.path.dirname(os.path.realpath(__file__))
GRAPH_DATA_FILE = "graph_data.pickle"
MONTHS = mdates.MonthLocator()
DAYS = mdates.DayLocator()
FORMAT = mdates.DateFormatter('%b %Y')
primary_crypto = config.primary_crypto
primary_real = config.primary_real


def generate(force_redraw=False):
    if os.path.isfile(f"{GRAPH_DATA_FOLDER}/{GRAPH_DATA_FILE}") and not force_redraw:
        invested, profit, dates, last_date = pickle.load(open(f"{GRAPH_DATA_FOLDER}/{GRAPH_DATA_FILE}", 'rb'))
    else:
        invested = []
        profit = []
        dates = []
        last_date = '1900-01-01'
    missing_dates = sorted(set(x[0][:10] for x in db.get_reports_dates_after(last_date)))
    for date in missing_dates:
        r_date, r = db.get_report(date)
        last_date = r_date
        dates.append(parser.parse(date))
        r = [list(x) for x in r]
        spent = -db.get_currency_state(r_date, db.get_primary_real())
        for i in range(len(r)):
            id, ticker, amount, exchanged = r[i]
            pr_bougth_amount = max(0, db.get_currency_in_real(r_date, id))
            if ticker == primary_crypto:
                in_pc = amount
                in_pr = exchanged
                pr_pc = in_pr / in_pc
            else:
                in_pc = exchanged
                in_pr = in_pc * pr_pc
            r[i].extend([in_pc, in_pr, pr_bougth_amount])
            del r[i][0]
            del r[i][2]
        invested.append(spent)
        profit.append(sum(x[3] for x in r) - spent)
    pickle.dump((invested, profit, dates, last_date), open(f"{GRAPH_DATA_FOLDER}/{GRAPH_DATA_FILE}", 'wb'))
    fig, ax = plt.subplots()
    ax.stackplot(
        dates,
        invested,
        profit,
        baseline="zero",
        labels=[f"Invested in {primary_real}", f"Profit in {primary_real}"],
        colors=('#085DAD', '#8CC739')
    )
    ax.xaxis.set_major_locator(MONTHS)
    ax.xaxis.set_major_formatter(FORMAT)
    ax.xaxis.set_minor_locator(DAYS)
    ax.legend(loc=2)
    ax.set_xlim(dates[0], dates[-1])
    maxy = max(x+y for x,y in zip(profit, invested))
    ax.set_ylim(0, maxy*1.1)
    plt.ylabel(primary_real)
    plt.title("Cryptocurrency investments")
    plt.grid(True)
    fig.autofmt_xdate()
    plt.show()
