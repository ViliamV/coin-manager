from tabulate import tabulate
import config
import os
import sqlite3

connection = sqlite3.connect('{}/database.db'.format(os.path.dirname(os.path.realpath(__file__))))
cursor = connection.cursor()

def get_last_rows_id(table):
    cursor.execute("SELECT id FROM {} ORDER BY id DESC LIMIT 1".format(table))
    return cursor.fetchone()[0]

def insert(table, values):
    insertstr = "INSERT INTO {} VALUES (NULL{})".format(table, len(values) * ',?')
    cursor.execute(insertstr, values)
    return get_last_rows_id(table)

def add_exchange(name):
    return insert('exchanges', (name,))

def add_currency(name, ticker, notes='', exchange_id=None):
    return insert('currencies', (name, ticker, notes, exchange_id))

def add_transaction(c_from, c_to, a_from, a_to, date):
    return insert('transactions', (c_from, c_to, a_from, a_to, date))

def add_report(date):
    return insert('reports', (date,))

def add_row(report_id, currency_id, amount, exchanged):
    return insert('report_rows', (report_id, currency_id, amount, exchanged))

def get_currency_id(currency):
    cursor.execute('SELECT id FROM currencies WHERE LOWER(name)=? OR LOWER(ticker)=?', (currency.lower(), currency.lower()))
    try:
        return cursor.fetchone()[0]
    except:
        return None

def get_currency_ticker(id):
    cursor.execute('SELECT ticker FROM currencies WHERE id=?', (id,))
    try:
        return cursor.fetchone()[0]
    except:
        return None

def get_exchange_id(exchange):
    cursor.execute('SELECT id from exchanges WHERE lower(name)=?', (exchange.lower(),))
    try:
        return cursor.fetchone()[0]
    except:
        return None
    return None

def get_all(table):
    return cursor.execute('SELECT * FROM {}'.format(table)).fetchall()

def get_reports_dates_after(date):
    return cursor.execute('''SELECT date
                             FROM reports
                             WHERE date(date) > date(?)
                             ORDER BY date ASC''', (date, )).fetchall()

def get_all_transactions():
    return tuple(cursor.execute('''SELECT t.id, c1.ticker, c2.ticker, t.amount_from, t.amount_to, t.date
                                    FROM (transactions AS t INNER JOIN currencies AS c1 ON t.currency_from = c1.id)
                                        INNER JOIN currencies AS c2 ON t.currency_to = c2.id'''))

def get_all_currencies():
    return get_all('currencies')

def get_all_exchanges():
    return get_all('exchanges')

def get_currencies_for_report():
    real = get_primary_real()
    return tuple(cursor.execute('''SELECT c.id, c.ticker, s.name
                                    FROM currencies as c INNER JOIN exchanges as s
                                    ON c.exchange_id = s.id
                                    WHERE c.id<>?''', (get_primary_real(),)))

def get_report(date):
    last_report_from_date = tuple(cursor.execute('''SELECT id, date
                                                 from reports
                                                 where date(date)<=date(?)
                                                 ORDER BY date DESC''', (date, )))
    if len(last_report_from_date) > 0:
        r_id = last_report_from_date[0][0]
        r_date = last_report_from_date[0][1]
        rows = cursor.execute('''SELECT r.currency_id, c.ticker, r.amount, r.exchanged
                                    FROM report_rows as r INNER JOIN currencies as c
                                    ON r.currency_id = c.id
                                    WHERE report_id=?
                                    ORDER BY currency_id ASC''', (r_id, )).fetchall()
        return r_date, rows
    return []

def get_currency_state(date, id):
    bought = tuple(cursor.execute('''SELECT sum(amount_to)
                            FROM transactions
                            WHERE currency_to=? AND datetime(date)<=datetime(?)''', (id, date)))[0][0]
    sold = tuple(cursor.execute('''SELECT sum(amount_from)
                          FROM transactions
                          WHERE currency_from=? AND datetime(date)<=datetime(?)''', (id, date)))[0][0]
    bought = 0 if bought is None else bought
    sold = 0 if sold is None else sold
    return(bought - sold)

def get_cached_state(date, id):
    cursor.execute("""CREATE TABLE IF NOT EXISTS currency_state_cache (
                   id INTEGER PRIMARY KEY,
                   date TEXT NOT NULL,
                   currency INTEGER,
                   state REAL NOT NULL,
                   FOREIGN KEY (currency) REFERENCES currencies(id)
                   )""")
    last = cursor.execute("SELECT state, date FROM currency_state_cache WHERE currency={} AND datetime(date)<=datetime('{}') ORDER BY datetime(date) DESC LIMIT 1".format(id, date)).fetchone()
    if last:
        return last
    else:
        return (0, None)

def drop_cache_table():
    cursor.execute("""DROP TABLE IF EXISTS currency_state_cache""")

def get_currency_in_real(date, id):
    date = date[:23]
    state, from_date = get_cached_state(date, id)
    real_id = get_primary_real()
    querystring = "SELECT amount_from, currency_from, date, amount_to FROM transactions WHERE datetime(date)<=datetime('{}')".format(date)
    if from_date:
        from_date = from_date[:23]
        querystring += " AND datetime(date)>datetime('{}')".format(from_date)
    b_querystring = querystring + " AND currency_to={} ORDER BY date ASC".format(id)
    s_querystring = querystring + " AND currency_from={} ORDER BY date ASC".format(id)
    buying_trans = cursor.execute(b_querystring).fetchall()
    state_changed = False
    for amount_from, currency_from, d, amount_to in buying_trans:
        state_changed = True
        if currency_from == real_id:
            state += amount_from
        else:
            result = get_real_price(d, currency_from, amount_from)
            state += result
    selling_trans = cursor.execute(s_querystring).fetchall()
    for amount_to, currency_to, d, amount_from in selling_trans:
        state_changed = True
        if currency_to == real_id:
            state -= amount_to
        else:
            result = get_real_price(d, currency_to, amount_to)
            state -= result
    if state_changed:
        insert("currency_state_cache", (date, id, state))
    return state

def get_real_price(date, id, amount):
    if id == get_primary_real():
        return amount
    else:
        r = tuple(cursor.execute('''SELECT r.date, rr.currency_id, rr.amount, rr.exchanged
                             FROM reports as r INNER JOIN report_rows as rr
                             ON r.id == rr.report_id
                             WHERE datetime(r.date)>=datetime(?) AND rr.currency_id==?
                             ORDER BY r.date ASC LIMIT 1''', (date, id)))
        if len(r) == 0:
            r = tuple(cursor.execute('''SELECT r.date, rr.currency_id, rr.amount, rr.exchanged
                     FROM reports as r INNER JOIN report_rows as rr
                     ON r.id == rr.report_id
                     WHERE datetime(r.date)<=datetime(?) AND rr.currency_id==?
                     ORDER BY r.date DESC LIMIT 1''', (date, id)))
        if len(r) == 0:
            return 0
        r = r[0]
        if id == get_primary_crypto():
            return amount*(r[3]/r[2])
        else:
            return amount*(get_real_price(r[0], get_primary_crypto(), r[3])/r[2])

def get_primary_real():
    try:
        return cursor.execute("SELECT id FROM currencies WHERE ticker==?", (config.primary_real, )).fetchone()[0]
    except:
        return None

def get_primary_crypto():
    try:
        return cursor.execute("SELECT id FROM currencies WHERE ticker==?", (config.primary_crypto, )).fetchone()[0]
    except:
        return None

def delete(table, id):
    return cursor.execute('DELETE FROM ? WHERE id=?', (table, id)).rowcount

def delete_transaction(id):
    return delete('transactions', id)

def commit_and_close():
    connection.commit()
    connection.close()

def commit():
    connection.commit()
